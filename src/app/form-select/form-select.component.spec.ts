import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormSelectComponent } from './form-select.component';

describe('FormInputComponent', () => {
  let component: FormSelectComponent;
  let fixture: ComponentFixture<FormSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormSelectComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FormSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
