import { Component, Input, Output, EventEmitter, HostListener, ViewChild, ElementRef } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { nanoid } from 'nanoid';

export interface IFormSelectOption {
  text: string,
  value: string
}

@Component({
  selector: 'app-form-select',
  standalone: true,
  imports: [FormsModule],
  templateUrl: './form-select.component.html',
  styleUrl: './form-select.component.scss'
})
export class FormSelectComponent {
  @ViewChild('formSelect') formSelect!: ElementRef;

  @Input() model!: string
  @Output() modelChange: EventEmitter<string> = new EventEmitter()

  @Input() label!: string
  @Input() placeholder!: string
  @Input() options!: IFormSelectOption[]

  selectedOption!: IFormSelectOption
  isOpen: boolean = false
  isDirty: boolean = false

  id = `select-${nanoid()}`

  toggleOpen() {
    this.isOpen = !this.isOpen
    if (!this.isDirty) this.isDirty = true
  }

  handleSelectOption(option: IFormSelectOption) {
    this.model = option.value
    this.selectedOption = option

    this.modelChange.emit(option.value)
  }

  @HostListener('document:click', ['$event'])
  onDocumentClick(event: MouseEvent) {
    if (!this.formSelect.nativeElement.contains(event.target)) {
      this.isOpen = false
    }
  }
}
