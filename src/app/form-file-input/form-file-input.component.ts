import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { nanoid } from 'nanoid';

@Component({
  selector: 'app-form-file-input',
  standalone: true,
  imports: [FormsModule],
  templateUrl: './form-file-input.component.html',
  styleUrl: './form-file-input.component.scss'
})
export class FormFileInputComponent {
  @Input() model!: File[]
  @Output() modelChange: EventEmitter<File[]> = new EventEmitter()

  @Input() label!: string
  @Input() multiple: boolean = false
  @Input() required: boolean = false

  id = `file-input-${nanoid()}`

  handleFileChange(ev: Event) {
    const input = ev.target as HTMLInputElement
    const files = input.files as FileList

    console.log('change', files)

    this.modelChange.emit([...files])
  }
}
