import { Component, ElementRef, ViewChild, AfterViewInit  } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { gsap } from 'gsap';
import { LinksListComponent } from '../links-list/links-list.component';
import { FormInputComponent } from '../form-input/form-input.component';
import { FormSelectComponent, IFormSelectOption } from '../form-select/form-select.component';
import { FormTextareaComponent } from '../form-textarea/form-textarea.component';
import { FormFileInputComponent } from '../form-file-input/form-file-input.component';

export interface ILink {
  href: string
  text: string
}
interface ILinks {
  title: string
  links: ILink[][]
}

interface IForm {
  fullName: string
  companyName: string
  phoneNumber: string
  email: string
  requestType: string
  projectDescription: string
  attachments: File[]
}

@Component({
  selector: 'app-footer',
  standalone: true,
  imports: 
  [
    LinksListComponent,
    FormInputComponent,
    FormSelectComponent,
    FormTextareaComponent,
    FormFileInputComponent,
    FormsModule
  ],
  templateUrl: './footer.component.html',
  styleUrl: './footer.component.scss'
})
export class FooterComponent implements AfterViewInit {
  @ViewChild('footer') footer!: ElementRef;
  @ViewChild('footerText') footerText!: ElementRef;
  @ViewChild('footerInfo') footerInfo!: ElementRef;
  @ViewChild('footerForm') footerForm!: ElementRef;
  @ViewChild('footerFormElement') footerFormElement!: ElementRef;
  @ViewChild('footerFormCloseLink') footerFormCloseLink!: ElementRef;

  timeline: gsap.core.Timeline = gsap.timeline()
  form: IForm = {
    fullName: '',
    companyName: '',
    phoneNumber: '',
    email: '',
    requestType: '',
    projectDescription: '',
    attachments: []
  }

  formOptions: IFormSelectOption[] = [
    {
      text: 'Partnership',
      value: 'partnership'
    },
    {
      text: 'Service request',
      value: 'service'
    },
    {
      text: 'Career',
      value: 'career'
    }
  ]

  menuLinks: ILinks[] = [
    {
      title: 'Pages',
      links: [
        [
          {
            href: '#',
            text: 'About'
          },
          {
            href: '#',
            text: 'EZ Smartboxes'
          },
          {
            href: '#',
            text: 'EZ Smartgrid'
          },
        ],
        [
          {
            href: '#',
            text: 'Our cases'
          },
          {
            href: '#',
            text: 'News'
          },
          {
            href: '#',
            text: 'Careers'
          },
        ]
      ]
    },
    {
      title: 'Social media',
      links: [
        [
          {
            href: '#',
            text: 'Telegram'
          },
          {
            href: '#',
            text: 'Facebook'
          },
          {
            href: '#',
            text: 'Instagram'
          },
        ],
        [
          {
            href: '#',
            text: 'Twitter'
          },
          {
            href: '#',
            text: 'Medium'
          },
          {
            href: '#',
            text: 'Linkedin'
          },
        ]
      ]
    },
  ]

  ngAfterViewInit() {
    this.timeline.pause()
      .to(this.footerText.nativeElement, {
        translateX: 264,
        duration: 0.4,
        ease: 'power1.inOut'
      })
      .to(this.footerInfo.nativeElement, {
        opacity: 0,
        duration: 0.2,
        ease: 'power1.inOut',
        onComplete: () => {
          this.footerInfo.nativeElement.style.display = 'none'
          this.footerText.nativeElement.classList.add('--blue')
        },
        onReverseComplete: () => {
          this.footerInfo.nativeElement.style.display = 'block'
        }
      })
      .to(this.footerForm.nativeElement, {
        opacity: 1,
        duration: 0.25,
        onStart: () => {
          this.footerForm.nativeElement.style.display = 'block'
        },
        onComplete: () => {
          this.footer.nativeElement.classList.add('--light')
          this.footerForm.nativeElement.classList.add('--relative')
        },
        onReverseComplete: () => {
          this.footerForm.nativeElement.classList.remove('--relative')
          this.footer.nativeElement.classList.remove('--light')
          this.footerForm.nativeElement.style.display = 'none'
          this.footerText.nativeElement.classList.remove('--blue')
        }
      })
      .to([this.footerFormElement.nativeElement, this.footerFormCloseLink.nativeElement], {
        opacity: 1,
        duration: 0.5,
        '--go-back-gradient-position': '0%',
        onStart: () => {
          this.footerFormCloseLink.nativeElement.classList.add('--animate')
          this.footerFormCloseLink.nativeElement.classList.add('--stroke')
        },
        onComplete: () => {
          this.footer.nativeElement.classList.add('--open')
          this.footerFormCloseLink.nativeElement.classList.remove('--animate')
        },
        onReverseComplete: () => {
          this.footer.nativeElement.classList.remove('--open')
          this.footerFormCloseLink.nativeElement.classList.remove('--stroke')
        }
      })
  }

  handleOpenForm() {
    this.timeline.play()

  }

  handleCloseForm() {
    this.timeline.reverse()
  }

  handleFormSubmit() {
    console.log('Form submit', this.form)
  }
}
