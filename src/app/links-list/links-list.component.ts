import { Component, Input } from '@angular/core';
import { ILink } from '../footer/footer.component';

@Component({
  selector: 'app-links-list',
  standalone: true,
  imports: [],
  templateUrl: './links-list.component.html',
  styleUrl: './links-list.component.scss'
})
export class LinksListComponent {
  @Input() title!: string;
  @Input() linksList!: ILink[][]
}
