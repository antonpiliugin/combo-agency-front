import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { nanoid } from 'nanoid';

@Component({
  selector: 'app-form-input',
  standalone: true,
  imports: [FormsModule],
  templateUrl: './form-input.component.html',
  styleUrl: './form-input.component.scss'
})
export class FormInputComponent {
  @Input() model!: string
  @Output() modelChange: EventEmitter<string> = new EventEmitter()

  @Input() label!: string
  @Input() placeholder!: string
  @Input() required: boolean = false

  id = `input-${nanoid()}`
}
