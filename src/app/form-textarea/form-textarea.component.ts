import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { nanoid } from 'nanoid';

@Component({
  selector: 'app-form-textarea',
  standalone: true,
  imports: [FormsModule],
  templateUrl: './form-textarea.component.html',
  styleUrl: './form-textarea.component.scss'
})
export class FormTextareaComponent {
  @Input() model!: string
  @Output() modelChange: EventEmitter<string> = new EventEmitter()

  @Input() label!: string
  @Input() placeholder!: string

  id = `textarea-${nanoid()}`
}
